
(function() {

    /*
    * initialize needed objects
    */

    let appear = new Appear(),
        mainNav = new Stick('.main-nav'),
        ticking = false;

    mainNav.scrollUpdate();

    /*
    * scroll event handling with throttle
    */

    function scrollUpdate() {
        if(!ticking) {
            window.requestAnimationFrame(function() {
                appear.scrollUpdate();
                mainNav.scrollUpdate();
                ticking = true;
            });
        }
        ticking = false;
    };

    window.addEventListener('scroll', scrollUpdate, false);

    /*
    * Menu toggle
    */

    let menuButton = document.querySelector('.main-nav__menu-button'),
        links = document.querySelector('.main-nav__menu__links'),
        menu = document.querySelector('.main-nav__menu'),
        body = document.querySelector('body'),
        html = document.querySelector('html');

    // listeners
    menuButton.addEventListener('click', function(e) {
        e.stopPropagation();
        e.preventDefault();
        toggleMenu(e);
    }, false);
    links.addEventListener('click', closeMenu, false);

    // toggle menu, stop propagation from the html listener
    function toggleMenu(e) {
        menu.classList.toggle('active');
        body.classList.toggle('active');
        html.classList.toggle('active');
    };

    function closeMenu(e) {
        menu.classList.remove('active');
        body.classList.remove('active');
        html.classList.remove('active');
    };

    /*
    * Init external modules
    */

    smoothScroll.init();
})();
