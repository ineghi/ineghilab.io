var renderer, scene, camera,
    group_spheres, mouseX, mouseY, mouseState,
    sphereVelocity = 4,
    spherePerimeter = 20,
    containerWidth = window.innerWidth,
    containerHeight = window.innerHeight,
    containerSemiWidth = window.innerWidth/2,
    containerSemiHeight = window.innerHeight/2;

init();
animate();
//listeners();

function init() {
    var container;

    //set up the container
    container = document.querySelector('.anim');

    //create the scene
    scene = new THREE.Scene();

    //set up the camera
    camera = new THREE.PerspectiveCamera(80, containerWidth / containerHeight, 1, 2500);
    camera.position.x = containerSemiWidth;
    camera.position.y = containerSemiHeight;
    camera.position.z = 1200;
    scene.add(camera);

    // create the spheres materials
    var SphereMaterial = new THREE.MeshLambertMaterial({
        color: 0xE7380B,
        emissive: 0xE7380B
    });

    // create the spheres
    group_spheres = new THREE.Group();

    var count = 150;
    while(count>0) {
        var sphere = new THREE.Mesh(
            new THREE.SphereGeometry(3, 18, 18),
            SphereMaterial
        );
        sphere.position.x = Math.random() * (containerWidth);
        sphere.position.y = Math.random() * (containerHeight);
        sphere.position.z = Math.random() * 1000;
        sphere.nextStep = calculateNextSphereStep(sphere.position.x, sphere.position.y, sphere.position.z);

        calculateSphereVelocity(sphere);

        group_spheres.add(sphere);
        count--;
    }

    scene.add(group_spheres);

    renderer = new THREE.WebGLRenderer({alpha:true});
    renderer.setClearColor( 0x15116D, 1);
    renderer.setSize(containerWidth, containerHeight);
    renderer.setPixelRatio(window.devicePixelRatio);

    container.appendChild(renderer.domElement);
}

function calculateNextSphereStep(x, y, z) {
    var step = {};

    if(mouseState) {
        step.distance = {
            x: mouseX - x,
            y: mouseY - y,
            z: 700 - z
        }
    } else {
        step.distance = {
            x: (Math.random() * spherePerimeter)*(Math.random() < 0.5 ? -1 : 1),
            y: (Math.random() * spherePerimeter)*(Math.random() < 0.5 ? -1 : 1),
            z: (Math.random() * spherePerimeter)*(Math.random() < 0.5 ? -1 : 1)
        }
    }
    step.totalDistance = Math.abs(step.distance.x) + Math.abs(step.distance.y) + Math.abs(step.distance.z);

    if((x+step.distance.x) < 0 || x+step.distance.x > containerWidth) {
        step.distance.x = 0;
    }

    if(y+step.distance.y < 0 || y+step.distance.y > containerHeight) {
        step.distance.y = 0;
    }

    if(z+step.distance.z < 0 || z+step.distance.z > 2000) {
        step.distance.x = 0;
    }

    return step;
}

function calculateSphereVelocity(sphere) {
    if(sphere.velocity == undefined) {
        sphere.velocity = {
            x: 0,
            y: 0,
            z: 0
        }
    }

    var tween = new TWEEN.Tween(sphere.velocity)
        .to({
            x: (sphere.nextStep.distance.x/sphere.nextStep.totalDistance) * sphereVelocity,
            y: (sphere.nextStep.distance.y/sphere.nextStep.totalDistance) * sphereVelocity,
            z: (sphere.nextStep.distance.z/sphere.nextStep.totalDistance) * sphereVelocity
        }, 1000)
        .onUpdate(function() {

        })
        .start();
}

function moveSpheres() {
    group_spheres.children.forEach(function(element) {
        element.position.x += element.velocity.x;
        element.position.y += element.velocity.y;
        element.position.z += element.velocity.z;

        element.nextStep.totalDistance -= sphereVelocity;

        if(element.nextStep.totalDistance <= 0) {
            element.nextStep = calculateNextSphereStep(element.position.x, element.position.y, element.position.z);
            calculateSphereVelocity(element);

        }
    })
}

function listeners() {
    document.addEventListener('mousemove', function(e) {
        mouseX = e.clientX;
		mouseY = containerHeight-e.clientY;
        mouseState = true;
        sphereVelocity+7;
    }, false)

    document.addEventListener('mouseout', function(e) {
        mouseState = false;
    }, false)

    document.addEventListener('resize', function() {
        containerSemiWidth = window.innerWidth/2;
        containerSemiHeight = window.innerHeight/2;

        camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();

		renderer.setSize(window.innerWidth, window.innerHeight);
    }, false)
}

function animate() {
    window.requestAnimationFrame(animate);
    render();
}

function render(time) {
    TWEEN.update(time);
    moveSpheres();
    renderer.render(scene, camera);
}
