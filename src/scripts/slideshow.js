class Slideshow {
    constructor(thumbs, slideshow) {
        this.thumbs = thumbs;
        this.slideshow = slideshow;
        this.html = document.querySelector('html');
        this.current = 0;
        this.buttonClose = this.slideshow.querySelector('.js-close');
        this.buttonPrev = this.slideshow.querySelector('.js-prev');
        this.buttonNext = this.slideshow.querySelector('.js-next');

        this.listeners();
    }

    defineCurrent(target) {
        let slideShowArray = Array.prototype.slice.call(this.thumbs);
        this.current = slideShowArray.indexOf(target);
    }

    loadImage() {
        let url = this.thumbs[this.current].getAttribute("data-slideShow");
        console.log(url);
        this.slideshow.style.backgroundImage = 'url("'+url+'")';
    }

    changeImage(value) {
        this.current += value;

        if(this.current == -1) {
            this.current = 0;
        } else if(this.current >= this.thumbs.length-1) {
            this.current = this.thumbs.length-1;
        }

        this.displayArrows();
        this.loadImage();
    }

    showSlideShow(target) {
        this.defineCurrent(target);
        this.changeImage(0);
        this.slideshow.classList.add('active');
        this.html.classList.add('active');
    }

    hideSlideShow() {
        this.slideshow.classList.remove('active');
        this.html.classList.remove('active');

    }

    displayArrows() {
        if(this.current > 0) {
            this.buttonPrev.classList.add('active');
        } else {
            this.buttonPrev.classList.remove('active');
        }

        if(this.current < this.thumbs.length -1) {
            this.buttonNext.classList.add('active');
        } else {
            this.buttonNext.classList.remove('active');
        }
    }

    listeners() {
        for (var i = 0; i < this.thumbs.length; i++) {
            this.thumbs[i].addEventListener('click', (e) => {
                this.showSlideShow(e.target);
            }, false)
        }

        this.buttonClose.addEventListener('click', () => {
            this.hideSlideShow();
        }, false)

        document.addEventListener('keypress', (e) => {
            if (e.keyCode == 27) {
                this.hideSlideShow();
            }
        }, false)

        this.buttonPrev.addEventListener('click', () => {
            this.changeImage(-1);
        }, false)

        document.addEventListener('keypress', (e) => {
            if (e.keyCode == 37) {
                this.changeImage(-1);
            }
        }, false)

        this.buttonNext.addEventListener('click', () => {
            this.changeImage(1);
        }, false)

        document.addEventListener('keypress', (e) => {
            if (e.keyCode == 39) {
                this.changeImage(1);
            }
        }, false)
    }
}

let thumbs = document.querySelectorAll('.gallery__item img'),
    domSlideshow = document.querySelector('.js-slideshow');

if(thumbs.length > 0) {
    let slideshow = new Slideshow(thumbs, domSlideshow);
}
