!function (e, n, t) { function o(e, n) { return typeof e === n; } function r() { var e, n, t, r, s, i, a; for (var l in x)
    if (x.hasOwnProperty(l)) {
        if (e = [], n = x[l], n.name && (e.push(n.name.toLowerCase()), n.options && n.options.aliases && n.options.aliases.length))
            for (t = 0; t < n.options.aliases.length; t++)
                e.push(n.options.aliases[t].toLowerCase());
        for (r = o(n.fn, "function") ? n.fn() : n.fn, s = 0; s < e.length; s++)
            i = e[s], a = i.split("."), 1 === a.length ? Modernizr[a[0]] = r : (!Modernizr[a[0]] || Modernizr[a[0]] instanceof Boolean || (Modernizr[a[0]] = new Boolean(Modernizr[a[0]])), Modernizr[a[0]][a[1]] = r), h.push((r ? "" : "no-") + a.join("-"));
    } } function s(e) { var n = T.className, t = Modernizr._config.classPrefix || ""; if (b && (n = n.baseVal), Modernizr._config.enableJSClass) {
    var o = new RegExp("(^|\\s)" + t + "no-js(\\s|$)");
    n = n.replace(o, "$1" + t + "js$2");
} Modernizr._config.enableClasses && (n += " " + t + e.join(" " + t), b ? T.className.baseVal = n : T.className = n); } function i() { return "function" != typeof n.createElement ? n.createElement(arguments[0]) : b ? n.createElementNS.call(n, "http://www.w3.org/2000/svg", arguments[0]) : n.createElement.apply(n, arguments); } function a(e) { return e.replace(/([a-z])-([a-z])/g, function (e, n, t) { return n + t.toUpperCase(); }).replace(/^-/, ""); } function l() { var e = n.body; return e || (e = i(b ? "svg" : "body"), e.fake = !0), e; } function c(e, t, o, r) { var s, a, c, u, f = "modernizr", d = i("div"), p = l(); if (parseInt(o, 10))
    for (; o--;)
        c = i("div"), c.id = r ? r[o] : f + (o + 1), d.appendChild(c); return s = i("style"), s.type = "text/css", s.id = "s" + f, (p.fake ? p : d).appendChild(s), p.appendChild(d), s.styleSheet ? s.styleSheet.cssText = e : s.appendChild(n.createTextNode(e)), d.id = f, p.fake && (p.style.background = "", p.style.overflow = "hidden", u = T.style.overflow, T.style.overflow = "hidden", T.appendChild(p)), a = t(d, e), p.fake ? (p.parentNode.removeChild(p), T.style.overflow = u, T.offsetHeight) : d.parentNode.removeChild(d), !!a; } function u(e, n) { return !!~("" + e).indexOf(n); } function f(e, n) { return function () { return e.apply(n, arguments); }; } function d(e, n, t) { var r; for (var s in e)
    if (e[s] in n)
        return t === !1 ? e[s] : (r = n[e[s]], o(r, "function") ? f(r, t || n) : r); return !1; } function p(e) { return e.replace(/([A-Z])/g, function (e, n) { return "-" + n.toLowerCase(); }).replace(/^ms-/, "-ms-"); } function v(n, o) { var r = n.length; if ("CSS" in e && "supports" in e.CSS) {
    for (; r--;)
        if (e.CSS.supports(p(n[r]), o))
            return !0;
    return !1;
} if ("CSSSupportsRule" in e) {
    for (var s = []; r--;)
        s.push("(" + p(n[r]) + ":" + o + ")");
    return s = s.join(" or "), c("@supports (" + s + ") { #modernizr { position: absolute; } }", function (e) { return "absolute" == getComputedStyle(e, null).position; });
} return t; } function m(e, n, r, s) { function l() { f && (delete z.style, delete z.modElem); } if (s = o(s, "undefined") ? !1 : s, !o(r, "undefined")) {
    var c = v(e, r);
    if (!o(c, "undefined"))
        return c;
} for (var f, d, p, m, y, g = ["modernizr", "tspan"]; !z.style;)
    f = !0, z.modElem = i(g.shift()), z.style = z.modElem.style; for (p = e.length, d = 0; p > d; d++)
    if (m = e[d], y = z.style[m], u(m, "-") && (m = a(m)), z.style[m] !== t) {
        if (s || o(r, "undefined"))
            return l(), "pfx" == n ? m : !0;
        try {
            z.style[m] = r;
        }
        catch (h) { }
        if (z.style[m] != y)
            return l(), "pfx" == n ? m : !0;
    } return l(), !1; } function y(e, n, t, r, s) { var i = e.charAt(0).toUpperCase() + e.slice(1), a = (e + " " + E.join(i + " ") + i).split(" "); return o(n, "string") || o(n, "undefined") ? m(a, n, r, s) : (a = (e + " " + R.join(i + " ") + i).split(" "), d(a, n, t)); } function g(e, n, o) { return y(e, t, t, n, o); } var h = [], x = [], C = { _version: "3.3.1", _config: { classPrefix: "", enableClasses: !0, enableJSClass: !0, usePrefixes: !0 }, _q: [], on: function (e, n) { var t = this; setTimeout(function () { n(t[e]); }, 0); }, addTest: function (e, n, t) { x.push({ name: e, fn: n, options: t }); }, addAsyncTest: function (e) { x.push({ name: null, fn: e }); } }, Modernizr = function () { }; Modernizr.prototype = C, Modernizr = new Modernizr, Modernizr.addTest("eventlistener", "addEventListener" in e), Modernizr.addTest("geolocation", "geolocation" in navigator), Modernizr.addTest("queryselector", "querySelector" in n && "querySelectorAll" in n), Modernizr.addTest("svg", !!n.createElementNS && !!n.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGRect), Modernizr.addTest("svgfilters", function () { var n = !1; try {
    n = "SVGFEColorMatrixElement" in e && 2 == SVGFEColorMatrixElement.SVG_FECOLORMATRIX_TYPE_SATURATE;
}
catch (t) { } return n; }); var T = n.documentElement, b = "svg" === T.nodeName.toLowerCase(); Modernizr.addTest("canvas", function () { var e = i("canvas"); return !(!e.getContext || !e.getContext("2d")); }), Modernizr.addTest("video", function () { var e = i("video"), n = !1; try {
    (n = !!e.canPlayType) && (n = new Boolean(n), n.ogg = e.canPlayType('video/ogg; codecs="theora"').replace(/^no$/, ""), n.h264 = e.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/, ""), n.webm = e.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/, ""), n.vp9 = e.canPlayType('video/webm; codecs="vp9"').replace(/^no$/, ""), n.hls = e.canPlayType('application/x-mpegURL; codecs="avc1.42E01E"').replace(/^no$/, ""));
}
catch (t) { } return n; }), Modernizr.addTest("webgl", function () { var n = i("canvas"), t = "probablySupportsContext" in n ? "probablySupportsContext" : "supportsContext"; return t in n ? n[t]("webgl") || n[t]("experimental-webgl") : "WebGLRenderingContext" in e; }); var w = C._config.usePrefixes ? " -webkit- -moz- -o- -ms- ".split(" ") : ["", ""]; C._prefixes = w, Modernizr.addTest("csspositionsticky", function () { var e = "position:", n = "sticky", t = i("a"), o = t.style; return o.cssText = e + w.join(n + ";" + e).slice(0, -e.length), -1 !== o.position.indexOf(n); }); var S = C.testStyles = c; Modernizr.addTest("touchevents", function () { var t; if ("ontouchstart" in e || e.DocumentTouch && n instanceof DocumentTouch)
    t = !0;
else {
    var o = ["@media (", w.join("touch-enabled),("), "heartz", ")", "{#modernizr{top:9px;position:absolute}}"].join("");
    S(o, function (e) { t = 9 === e.offsetTop; });
} return t; }); var _ = "Moz O ms Webkit", E = C._config.usePrefixes ? _.split(" ") : []; C._cssomPrefixes = E; var P = function (n) { var o, r = w.length, s = e.CSSRule; if ("undefined" == typeof s)
    return t; if (!n)
    return !1; if (n = n.replace(/^@/, ""), o = n.replace(/-/g, "_").toUpperCase() + "_RULE", o in s)
    return "@" + n; for (var i = 0; r > i; i++) {
    var a = w[i], l = a.toUpperCase() + "_" + o;
    if (l in s)
        return "@-" + a.toLowerCase() + "-" + n;
} return !1; }; C.atRule = P; var R = C._config.usePrefixes ? _.toLowerCase().split(" ") : []; C._domPrefixes = R; var k = { elem: i("modernizr") }; Modernizr._q.push(function () { delete k.elem; }); var z = { style: k.elem.style }; Modernizr._q.unshift(function () { delete z.style; }), C.testAllProps = y; var L = C.prefixed = function (e, n, t) { return 0 === e.indexOf("@") ? P(e) : (-1 != e.indexOf("-") && (e = a(e)), n ? y(e, n, t) : y(e, "pfx")); }; Modernizr.addTest("intl", !!L("Intl", e)), C.testAllProps = g, function () { Modernizr.addTest("csscolumns", function () { var e = !1, n = g("columnCount"); try {
    (e = !!n) && (e = new Boolean(e));
}
catch (t) { } return e; }); for (var e, n, t = ["Width", "Span", "Fill", "Gap", "Rule", "RuleColor", "RuleStyle", "RuleWidth", "BreakBefore", "BreakAfter", "BreakInside"], o = 0; o < t.length; o++)
    e = t[o].toLowerCase(), n = g("column" + t[o]), ("breakbefore" === e || "breakafter" === e || "breakinside" == e) && (n = n || g(t[o])), Modernizr.addTest("csscolumns." + e, n); }(), Modernizr.addTest("flexbox", g("flexBasis", "1px", !0)), Modernizr.addTest("flexboxlegacy", g("boxDirection", "reverse", !0)), r(), s(h), delete C.addTest, delete C.addAsyncTest; for (var A = 0; A < Modernizr._q.length; A++)
    Modernizr._q[A](); e.Modernizr = Modernizr; }(window, document);
