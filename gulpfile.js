/*
 * Hello !
 *
 * Here's the most important tasks:
 *  - gulp serve (build the application, run a server(optionnal) and watch the files)
 *  - gulp build (simply build)
 */
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    base64 = require('gulp-base64'),
    connect = require('gulp-connect'),
    autoprefixer = require('gulp-autoprefixer'),
    imagemin = require('gulp-imagemin'),
    cache = require('gulp-cache'),
    plumber = require('gulp-plumber'),
    del = require('del'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    babel = require('gulp-babel'),
    sourcemaps = require('gulp-sourcemaps'),
    runSequence = require('run-sequence'),
    rev = require('gulp-rev'),
    revReplace = require('gulp-rev-replace'),
    gulpif = require('gulp-if'),
    useref = require('gulp-useref'),
    git = require('gulp-git'),
    bump = require('gulp-bump'),
    filter = require('gulp-filter'),
    tag_version = require('gulp-tag-version');
/*
 * Configuration object
 *
 * templates goes directly in de 'dest' folder, but the proprety allow to put the templates into a submodule
 */
var config = {
    dest: './static',
    templates: './layouts',
    serverPort: 9003
};
/*
 * Templates process
 */
gulp.task('templates', function () {
    var manifest = gulp.src(['.tmp/css/rev-manifest.json', '.tmp/scripts/rev-manifest.json']);
    return gulp.src('./src/templates/**/*')
        .pipe(useref({
            searchPath: ['./node_modules', './src']
        }))
        .pipe(gulpif('*.js', rev()))
        .pipe(gulpif('*.js', gulp.dest(config.dest)))
        .pipe(revReplace({manifest: manifest}))
        .pipe(gulpif(['**/*.html', '**/*.php'], gulp.dest(config.templates)))
        .pipe(connect.reload());
});
/*
 * Font process
 */
gulp.task('fonts', function() {
    gulp.src(['src/sass/fonts/**/*', '!src/sass/fonts/_fonts.scss'])
    .pipe(gulp.dest(config.dest+'/assets/fonts'));
})
/*
 * Javascript process
 *
 * Supports ES6 via Babel
 * Export into two files: One into main.js, and the other into vendors.js (vendors src files come from a /vendors folder)
 */
gulp.task('scripts', function () {
    return gulp.src('src/scripts/**/*.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(concat('main.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(uglify())
        .pipe(rev())
        .pipe(plumber())
        .pipe(gulp.dest(config.dest+'/assets/scripts'))
        .pipe(rev.manifest())
        .pipe(gulp.dest('.tmp/scripts'));
});
/*
 * Sass process
 */
gulp.task('styles', function () {
    return gulp.src('src/sass/**/*.scss')
        .pipe(plumber())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
        .pipe(base64())
        .pipe(rev())
        .pipe(plumber())
        .pipe(gulp.dest(config.dest+'/assets/css'))
        .pipe(rev.manifest())
        .pipe(gulp.dest('.tmp/css'));
});
/*
 * Images process
 */
gulp.task('images', function() {
    return gulp.src('src/img/**/*')
        .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
        .pipe(plumber())
        .pipe(gulp.dest(config.dest+'/assets/img'))
});
/*
 * Cleaning process
 */
gulp.task('clean', function(cb) {
   return del([
       config.dest+'/assets',
       config.templates+'/**/*.html',
       config.templates+'/**/*.php',
       '.tmp',
       '!./node_modules/**/*',
       '!./src/**/*'
       
   ]);
});
gulp.task('clean:templates', function(cb) {
   return del([
     config.templates + '/**/*.html',
     config.templates + '/**/*.php',
     '!./node_modules/**/*',
     '!./src/**/*'
   ]);
});
gulp.task('clean:scripts', function(cb) {
    return del(config.dest+'/assets/scripts');
});
gulp.task('clean:styles', function(cb) {
    return del(config.dest+'/assets/css');
});
gulp.task('clean:images', function(cb) {
    return del(config.dest+'/assets/img');
});
/*
 * Bumping process
 */
function inc(importance) {
    // get all the files to bump version in
    return gulp.src(['./package.json', './bower.json'])
        // bump the version number in those files
        .pipe(bump({type: importance}))
        // save it back to filesystem
        .pipe(gulp.dest('./'))
        // commit the changed version number
        .pipe(git.commit('bumps package version'))
        // read only one file to get the version number
        .pipe(filter('package.json'))
        // **tag it in the repository**
        .pipe(tag_version());
}
gulp.task('bump:patch', function() { return inc('patch'); })
gulp.task('bump:minor', function() { return inc('minor'); })
gulp.task('bump:major', function() { return inc('major'); })
/*
 * Local server stream
 */
gulp.task('startServer', function() {
    connect.server({
        root: config.dest,
        port: config.serverPort,
        livereload: true
    });
});
/*
 * Watch the files
 */
gulp.task('watch', function() {
    // Watch sass files
     gulp.watch('src/sass/**/*.scss', function() {
        runSequence('clean:styles', 'clean:templates', ['styles'], 'templates')
    });
    // Watch script files
    gulp.watch('src/scripts/**/*.js', function() {
        runSequence('clean:scripts', 'clean:templates', ['scripts'], 'templates')
    });
    // Watch image files
    gulp.watch('src/img/**/*', function() {
        runSequence('clean:images', 'clean:templates', ['images'], 'templates')
    });
    // Watch templates and generated files
    gulp.watch('src/templates/**/*.html', function() {
        runSequence('clean:templates', 'templates')
    });
});
/*
 *
 */
gulp.task('build', function() {
    runSequence('clean', ['fonts', 'styles', 'scripts', 'images'], 'templates');
});
/*
 * Build the app in dest, start the server and watch the files
 */
gulp.task('serve', ['clean'], function() {
    gulp.start('startServer', 'watch');
});
gulp.task('default', ['clean'], function() {
    console.log("'gulp serve': run a server and watch the files");
    console.log("'gulp watch': watch the files");
    console.log("'gulp build': just build");
    gulp.start('build', 'startServer', 'watch');
});